%global debug_package %{nil}

Name:           flang
Version:        flang_20210324
Release:        8
Summary:        Fortran language compiler targeting LLVM

License:        Apache-2.0
URL:            https://github.com/flang-compiler/flang
Source0:        flang-flang_20210324.tar.gz

BuildRequires:  gcc
Requires:       gcc >= 9.3.0
Patch0: 1-flang-runtime-inline.patch
Patch1: 2-inline_f90_str_copy_klen.patch
Patch2: 3-add-tests-interoperability-C.patch
Patch3: 4-add-test-cases-for-openmp-optimization.patch

%description
Flang depends on a fork of the LLVM project (https://github.com/flang-compiler/classic-flang-llvm-project). The fork made some changes to the upstream LLVM project to support Flang toolchain. Flang cannot build independently for now.
Link to build Flang based on the fork of LLVM project: https://github.com/flang-compiler/flang/wiki/Building-Flang
TODO: support build Flang.

%prep
%autosetup -p1


%build


%install


%files
%license LICENSE.txt


%changelog
* Mon Sep 19 2022 xieyihui <yihui@isrc.iscas.ac.cn> - flang_20210324-8
- Add patch for add test cases for OpenMP optimization

* Thu Sep 8 2022 a <yihui@isrc.iscas.ac.cn> - flang_20210324-7
- Add patch for add test cases for interoperability with C about fortran call C

* Thu Sep 8 2022 wangzhewei <51215902151@stu.ecnu.edu.cn> - flang_20210324-6
- Add patch for inline of runtime function inline_f90_str_copy_klen

* Fri Aug 26 2022 wangzhewei <51215902151@stu.ecnu.edu.cn> - flang_20210324-5
- Fix patch 1 to apply on flang-flang_20210324.tar.gz

* Thu Jul 14 2022 qiaopeixin <qiaopeixin@huawei.com> - flang_20210324-4
- Add patch for inline of runtime functions

* Wed May 18 2022 liukuo <liukuo@kylinos.cn> - flang_20210324-3
- License compliance rectification

* Mon Dec 13 2021 heyitao <heyitao@uniontech.com> - flang_20210324-2
- Remove the dist tag in the version.

* Thu Nov 25 2021 qiaopeixin <qiaopeixin@huawei.com> - flang_20210324-20211125.1
- Add flang source tar file

